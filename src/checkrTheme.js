export default {
  root: {
    height: '100%',
    margin: '0 auto',
    flexGrow: 1,
  },
  button: {
    margin: 12,
    boxShadow: '0',
  },
  appBar: {
    colorPrimary: { main: 'rgb(64,123,185)' },
    colorDefault: { main: 'rgb(64,123,185)' },
  },
  suspendedIcon: {
    color: 'gray',
  },
  pendingIcon: {
    color: 'gray',
  },
  considerIcon: {
    color: 'rgb(253,182,43)',
  },
  clearIcon: {
    color: 'limegreen',
  },
  palette: {
    primary: { main: 'rgb(64,123,185)' }, // demo primary
  },
  spacing: {
    unit: 10,
  },
  typography: {
    fontFamily:
      '"proxima-nova", "Helvetica Neue", Helvetica, Arial, sans-serif',
  },
};
