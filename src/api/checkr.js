import axios from 'axios';
import response from './response.json';

const originalUrl = `https://8z74to6yra.execute-api.us-east-1.amazonaws.com/production/candidates`;
const url =
  process.env.NODE_ENV === 'production'
    ? `https://us-central1-cors-proxy-68b2b.cloudfunctions.net/cors?url=${encodeURIComponent(
        originalUrl,
      )}`
    : `/production/candidates`;

export async function fetchInitialData() {
  if (process.env.NODE_ENV !== 'production') {
    return response;
  }
  const { data } = await axios.get(url, {
    responseType: 'json',
  });

  return data;
}
