export default {
  root: {
    maxWidth: 1020,
    margin: '0 auto',
    flexGrow: 1,
    paddingTop: 30,
    paddingRight: 30,
    paddingLeft: 30,
    paddingBottom: 30,
  },
  button: {
    margin: 12,
    boxShadow: '0',
  },
  buttonSubmit: {
    backgroundColor: '#006EFF',
    borderColor: '#006EFF',
    borderRadius: 100,
    color: '#FFFFFF',
    fontSize: 13,
    '&:hover': {
      backgroundColor: '#0069d9',
      borderColor: '#0062cc',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#0062cc',
      borderColor: '#005cbf',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
    },
  },
  card: {
    title: {
      textAlign: 'center',
      margin: '0 auto',
    },
    padding: 12,
  },
  input: {
    width: `calc(100% - ${16}px)`,
  },
  appBar: {
    backgroundImage:
      'linear-gradient(90deg, #017AFF 3%, #006EFF 16%, #059CFF 100%)',
    boxShadow: 'none',
  },

  // global theme below only (delete above as replaced)
  maxWidth: 1020,
  palette: {
    primary: { main: '#006eff' }, // demo primary
  },
  spacing: {
    unit: 10,
  },
  typography: {
    fontFamily:
      '"proxima-nova", "Helvetica Neue", Helvetica, Arial, sans-serif',
  },
};
