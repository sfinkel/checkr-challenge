import React, { Component } from 'react';
import moment from 'moment';
import { withStyles } from '@material-ui/core/styles';
import { lowerCase, pick, values } from 'lodash';
import { AppBar, Grid, Tab, Tabs, Typography, Button } from '@material-ui/core';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { EnhancedTable } from '../../components';
import { isLoadingSelector } from '../AppContainer/selectors';
import {
  getStatus,
  makeCreateReportDetails,
  getReportNameSelector,
} from './selectors';
import styles from './styles';

const columnData = [
  {
    id: 'label',
    numeric: false,
    disablePadding: true,
    label: 'LABEL',
  },
  {
    id: 'value',
    numeric: false,
    disablePadding: true,
    label: 'VALUE',
  },
];

const fields = [
  [
    'name',
    'dateOfBirth',
    'ssn',
    'driverLicense',
    'zipcode',
    'phone',
    'email',
    'customId',
  ],
  ['status', 'adjudication', 'assessment', 'turnaroundTime'],
  ['package', 'geo', 'completed', 'created'],
];

const hiddenFields = [
  ['name', 'dateOfBirth', 'ssn'],
  ['status', 'adjudication', 'assessment'],
  ['package', 'geo', 'completed'],
];

class ScrollableTabsButtonAuto extends Component {
  state = {
    value: 0,
    visible: true,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  formTabLabel = ({ status, updated } = {}) => {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <span>{moment(updated.value).format('MM/DD/YYYY')}</span>
        <span className={classes[`${lowerCase(status.value)}Icon`]}>
          {' • '}
        </span>
      </React.Fragment>
    );
  };

  handleClick = () => {
    return this.setState((prevState) => ({
      visible: !prevState.visible,
    }));
  };
  tableTitles = ['Applicant Details', 'Report Details'];

  makeTab = (details) => {
    const { classes } = this.props;

    return fields.map((field, idx) => {
      const showFields =
        this.state.visible === true ? fields[idx] : hiddenFields[idx];
      const rows = values(pick(details, showFields));
      return (
        <Grid
          item
          xs={12}
          sm={idx === 0 ? 12 : 6}
          md={4}
          key={`${field}-${idx}`}
        >
          <EnhancedTable
            columnData={columnData}
            rows={rows}
            enableCheckbox={false}
            elevation={0}
            rowsPerPage={rows.length}
            enableHeader={false}
            classes={{ tablePaper: classes.paper }}
          />
        </Grid>
      );
    });
  };

  formTabLabel = (details = {}) => {
    const { updated = {}, statusText = {} } = details;
    const { classes } = this.props;
    return (
      <React.Fragment>
        <span>{moment(updated.value).format('MM/DD/YYYY')}</span>
        <span className={classes[`${lowerCase(statusText.value)}Icon`]}>
          {' • '}
        </span>
      </React.Fragment>
    );
  };

  render() {
    const { classes, data = [], name, isLoading = false } = this.props;
    const { value, visible } = this.state;
    return !isLoading ? (
      <Grid container>
        <Grid item xs={12} sm={12} md={12} lg={12} className={classes.root}>
          <Typography
            variant="headline"
            color="primary"
            align="center"
            className={classes.title}
          >
            {name}
          </Typography>
          <AppBar
            position="static"
            elevation={0}
            className={classes.appBar}
            color="default"
          >
            <Tabs
              value={value}
              onChange={this.handleChange.bind(this)}
              indicatorColor="primary"
              centered={false}
              className={classes.tabRoot}
            >
              {data.map(({ details = {} }, idx) => {
                return <Tab key={idx} label={this.formTabLabel(details)} />;
              })}
            </Tabs>
          </AppBar>

          <Grid className={classes.tabContainer}>
            <Grid container justify="center" spacing={0}>
              {data.map(({ details = {} }, index) => {
                return (
                  value === index &&
                  fields.map((field, idx) => {
                    const showFields = visible
                      ? hiddenFields[idx]
                      : fields[idx];
                    const rows = values(pick(details, showFields));
                    return (
                      <Grid
                        item
                        xs={12}
                        sm={idx === 0 ? 12 : 6}
                        md={4}
                        key={`${field}-${idx}-${index}-${visible}`}
                      >
                        <EnhancedTable
                          columnData={columnData}
                          rows={rows}
                          enableCheckbox={false}
                          title={
                            <Typography variant="body2">
                              {this.tableTitles[idx]}
                            </Typography>
                          }
                          elevation={0}
                          rowsPerPage={rows.length}
                          enableHeader={false}
                          classes={{ tablePaper: classes.paper }}
                        />
                      </Grid>
                    );
                  })
                );
              })}
            </Grid>
          </Grid>
          <Grid container alignItems="center" justify="center">
            <Grid item>
              <Button
                variant="outlined"
                color="primary"
                className={classes.button}
                onClick={() => this.setState({ visible: !visible })}
              >
                {`Show ${this.state.visible ? 'more' : 'less'} details`}
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    ) : (
      <CircularProgress className={classes.progress} />
    );
  }
}

const makeMapStateToProps = (state, props) => {
  const createReportDetails = makeCreateReportDetails();
  const isLoading = isLoadingSelector();
  const mapStateToProps = (state, props) => {
    const { data } = createReportDetails(state, props);
    return {
      data,
      status: getStatus(state, props),
      name: getReportNameSelector(state, props),
      isLoading: isLoading(state, props),
    };
  };
  return mapStateToProps;
};

ScrollableTabsButtonAuto = withStyles(styles)(ScrollableTabsButtonAuto);
export default withRouter(
  connect(makeMapStateToProps)(ScrollableTabsButtonAuto),
);
