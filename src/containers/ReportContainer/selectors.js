import React, { Fragment } from 'react';
import { createSelector } from 'reselect';
import { startCase, keyBy, camelCase } from 'lodash';
import moment from 'moment';
import { formatNumber } from 'libphonenumber-js';
import { makeAppData, getStatus } from '../AppContainer/selectors';
import { CheckrStatusIcon, CheckrStatusWithIcon } from '../../components';
import NumbersOnly from '../../normalizers/fields/numbersOnly';
const NOT_AVAILABLE = '-';

const reportId = (state, props) => props.match.params.id;
const reportIdSelector = createSelector(reportId, (reportId) => reportId);
const getCurrentReportData = createSelector(
  [makeAppData(), getStatus, reportIdSelector],
  (data, status, reportId) => (data[status] || {})[reportId],
);
const currentReportSelector = createSelector(
  [getCurrentReportData],
  (currentReportData) => formatReport(currentReportData),
);
const otherReportSelector = createSelector(
  [getCurrentReportData, reportIdSelector],
  ({ item = {} } = {}, reportId) => {
    const { reports = [] } = item;
    return reports
      .filter(({ id }) => Number(id) !== Number(reportId))
      .map((report) => formatReport({ item, report }));
  },
);

const allReportsData = createSelector(
  [currentReportSelector, otherReportSelector],
  (currentReportData, otherReportData) => [
    currentReportData,
    ...otherReportData,
  ],
);

export const getReportNameSelector = createSelector(
  [getCurrentReportData],
  ({ item: { name } = {} } = {}) => name,
);

export const makeCreateReportDetails = () =>
  createSelector([allReportsData], (data) => {
    return { data };
  });

function maskSSN(ssn = '') {
  return `•••• •• ${NumbersOnly(ssn.substring(7))}`;
}

function formSSN(ssn) {
  return (
    <React.Fragment>
      {`${maskSSN(ssn)}`}
      <CheckrStatusIcon status="clear" style={{ paddingLeft: '5px' }} />
    </React.Fragment>
  );
}

function formatReport({ item = {}, report = {} } = {}) {
  const {
    name,
    dob,
    ssn,
    dl_state: dlState,
    dl_number: dlNumber,
    zipcode,
    phone,
    email,
    custom_id: customId,
  } = item;

  const {
    status,
    adjudication,
    assessment,
    package: pkg,
    geo,
    completed_at: completedAt,
    created_at: createdAt,
    updated_at: updatedAt,
  } = report;

  const turnaroundTime =
    createdAt && completedAt
      ? `${moment
          .duration(moment(completedAt).diff(moment(createdAt)))
          .asHours()} hours`
      : NOT_AVAILABLE;

  const details = keyBy(
    [
      {
        label: 'Name',
        value: name,
      },
      {
        label: 'Date of Birth',
        value: (
          <Fragment>
            {moment(dob).format('MM/DD/YYYY')}
            <span style={{ color: 'gray' }}>
              {` ${moment().diff(dob, 'years')}`}
              <span> years old</span>
            </span>
          </Fragment>
        ),
      },
      {
        label: 'SSN',
        value: formSSN(ssn),
      },
      {
        label: 'Driver License',
        value: `${dlNumber} (${dlState})`,
      },
      {
        label: 'Zipcode',
        value: zipcode,
      },
      {
        label: 'Phone',
        value: formatNumber({ country: 'US', phone: `${phone}` }, 'National'),
      },
      {
        label: 'Email',
        value: email,
      },
      {
        label: 'Custom ID',
        value: customId,
      },
      {
        label: 'Status',
        value: <CheckrStatusWithIcon status={status} />,
      },
      {
        label: 'StatusText',
        value: status,
      },
      {
        label: 'Adjudication',
        value: startCase(adjudication) || NOT_AVAILABLE,
      },
      {
        label: 'Assessment',
        value: startCase(assessment) || NOT_AVAILABLE,
      },
      {
        label: 'Turnaround Time',
        value: turnaroundTime,
      },
      {
        label: 'Package',
        value: startCase(pkg),
      },
      {
        label: 'Geo',
        value: startCase(geo),
      },
      {
        label: 'Completed',
        value: completedAt
          ? moment(completedAt).format('MM/DD/YYYY hh:mmA')
          : NOT_AVAILABLE,
      },
      {
        label: 'Created',
        value: moment(createdAt).format('MM/DD/YYYY HH:mmA'),
      },
      {
        label: 'Updated',
        value: moment(updatedAt).format('MM/DD/YYYY'),
      },
    ],
    ({ label }) => camelCase(label),
  );
  return {
    name,
    status,
    item,
    report,
    details,
  };
}

export { getStatus };
export default makeCreateReportDetails;
