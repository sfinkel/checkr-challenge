import grey from '@material-ui/core/colors/grey';

export default (theme) => {
  return {
    ...theme,
    root: {
      backgroundColor: 'rgb(247,249,251)',
      flexGrow: 1,
      width: '100%',
    },
    body: {
      '&:hover:hover': {
        cursor: 'text',
      },
    },
    secondaryHeading: {
      margin: '0 auto',
      padding: '0px',
    },
    paper: {
      height: '100%',
      backgroundColor: 'rgb(247,249,251)',
    },
    tabRoot: {},
    title: {
      minHeight: '32px',
      fontWeight: '600',
      fontSize: '20px',
      lineHeight: '2',
      backgroundColor: 'white',
      borderBottom: `1px solid ${grey[200]}`,
      padding: '10px',
    },
    button: {
      textTransform: 'initial',
      display: 'inline-block',
      position: 'relative',
      top: '-20px',
      backgroundColor: 'rgb(247,249,251)',
      paddingTop: '5px',
      paddingBottom: '5px',
      borderRadius: '20px',
      fontWeight: '400',
      '&:hover:hover': {
        backgroundColor: 'rgb(247,249,251)',
      },
    },
    tabContainer: {
      fontColor: 'black',
      padding: '20px 20px 40px 20px',
      borderBottom: `1px solid ${grey[200]}`,
    },
    appBar: {
      backgroundColor: 'white',
      color: 'black',
    },
  };
};
