import React, { Component } from 'react';
//import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { CheckrListTable, CheckrCountingAppBar } from '../../components';
import { listContainerSelector, isLoadingSelector } from './selectors';
import styles from './styles';

export class ListContainer extends Component {
  onClickRow = (ev, values) => {
    const { history = {}, status } = this.props;
    return history.push(`/lists/${status}/report/${values}`);
  };

  render() {
    const { data, classes, status } = this.props;
    return (
      <Grid container className={classes.root}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <CheckrListTable
            data={data}
            onClickRow={this.onClickRow}
            classes={classes}
            status={status}
            title={
              // re-using table component, so passing a title component this time
              <CheckrCountingAppBar classes={{ root: classes.appBarRoot }} />
            }
          />
        </Grid>
      </Grid>
    );
  }
}

const makeMapStateToProps = () => {
  const isLoading = isLoadingSelector();
  const mapStateToProps = (state, props) => {
    const { data, status } = listContainerSelector(state, props);
    return {
      data,
      status,
      isLoading: isLoading(state, props),
    };
  };
  return mapStateToProps;
};

ListContainer = withStyles(styles)(ListContainer);
export default connect(makeMapStateToProps)(ListContainer);
