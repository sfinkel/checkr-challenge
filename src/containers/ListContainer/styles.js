export default (theme) => ({
  root: {
    flexGrow: 1,
  },
  appBarRoot: {
    paddingTop: '20px',
  },
  tablePaper: {
    overflowX: 'auto',
  },
});
