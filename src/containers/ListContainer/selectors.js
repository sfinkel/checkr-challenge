import { createSelector } from 'reselect';
import {
  makeAppData,
  getStatus,
  isLoadingSelector,
} from '../AppContainer/selectors';

export { isLoadingSelector };
export const listContainerSelector = createSelector(
  [makeAppData(), getStatus],
  (data, status) => {
    return { data: data[status], status };
  },
);
