import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import CheckrListTile from '../../components/CheckrListTile';
import { isLoadingSelector } from '../AppContainer/selectors';
import styles from './styles';

class TilesContainer extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = { statuses: ['pending', 'consider', 'suspended'] };
  }

  onClickRow = (id, status) => {
    return this.props.history.push(`/lists/${status}/report/${id}`);
  };

  render() {
    const { classes, isLoading } = this.props;
    return (
      <Grid
        container
        className={classes.root}
        justify={!isLoading ? 'space-between' : 'center'}
        spacing={16}
      >
        {!isLoading ? (
          this.state.statuses.map((status) => {
            return (
              <Grid item xs={12} sm={4} md={4} key={status}>
                <CheckrListTile
                  onClickRow={(ev, id) => this.onClickRow(id, status)}
                  status={status}
                />
              </Grid>
            );
          })
        ) : (
          <Grid item>
            <CircularProgress className={classes.progress} />
          </Grid>
        )}
      </Grid>
    );
  }
}

function mapStateToProps(state, props) {
  const loading = isLoadingSelector();
  return { isLoading: loading(state, props) };
}

TilesContainer = withStyles(styles)(TilesContainer);
export default connect(mapStateToProps)(TilesContainer);
