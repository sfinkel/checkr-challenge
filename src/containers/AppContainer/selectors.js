import { createSelector } from 'reselect';
import { get, keys, map, reduce, orderBy } from 'lodash';
import moment from 'moment';

const isLoading = (state, props = {}) => state.app.isLoading;
const getAppData = (state = {}, props = {}) => {
  const { app: { data = {} } = {} } = state;
  return data;
};
export const getStatus = (state, props = {}) =>
  props.status ? props.status : get(props, 'match.params.status');

export const isLoadingSelector = () =>
  createSelector(isLoading, (isLoading) => isLoading);

export const appData = createSelector(getAppData, (data) => data);

export const getStatusSelector = () =>
  createSelector(getStatus, (status) => status);

export const makeAppData = () => {
  return createSelector([getAppData], (data) => transformData(data));
};

const transformedDataSelector = createSelector(
  [makeAppData(), getStatus],
  (data, status) => {
    return { data: getOrderedResultsForStatus(data, status), status };
  },
);

export const makeTransformedData = () => transformedDataSelector;

export const getTileColumnData = (state, props) => {};

export const makeTileColumnData = () =>
  createSelector(getStatus, (status) => formColumnData(status));

export const makeStatusCounts = () =>
  createSelector(makeAppData(), (data) =>
    reduce(
      keys(data),
      (acc, v) => {
        acc[v] = Object.keys(data[v]).length;
        acc['all'] += acc[v];
        return acc;
      },
      { all: 0 },
    ),
  );

function formColumnData(status) {
  return [
    {
      id: 'name',
      numeric: false,
      disablePadding: true,
      enableCheckbox: false,
      label: 'NAME',
    },
    {
      id: 'date',
      numeric: false,
      disablePadding: true,
      enableCheckbox: false,
      formatter: (val) =>
        val !== 'pending'
          ? moment(val).format('MM/DD/YYYY')
          : moment(val).format('MM/DD/YYYY'),
      label: status === 'pending' ? 'ESTIMATED DELIVERY' : 'UPDATED',
    },
  ];
}

function transformData(data) {
  return {
    ...reduce(
      data,
      (acc, item) => {
        map(item.reports, (report) => {
          const { status, id } = report;
          return ((acc[status] || (acc[status] = {}))[id] = {
            item,
            report,
          });
        });
        return acc;
      },
      {},
    ),
  };
}

function getOrderedResultsForStatus(data, status) {
  const dateKey =
    status === 'pending' ? 'estimated_completed_at' : 'updated_at';
  const mapped = map(
    data[status],
    ({ item: { name } = {}, report: { id, [dateKey]: date } = {} }) => {
      return {
        id,
        name,
        date,
      };
    },
  );
  return orderBy(mapped, ['date'], ['desc']);
}
