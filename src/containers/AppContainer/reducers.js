import { mapKeys } from 'lodash';
import initialState from '../../initialState';
import { GET_DATA, GET_DATA_FAIL, GET_DATA_SUCCESS } from './constants';
export default function appContainerReducer(
  state = initialState,
  { data = {}, message = '', type },
) {
  switch (type) {
    case GET_DATA:
      return { ...state, ...data, isLoading: true };
    case GET_DATA_FAIL:
      console.error(message);
      return state;
    case GET_DATA_SUCCESS:
      return { ...state, data: mapKeys(data, 'id'), isLoading: false };
    default:
      return state;
  }
}
