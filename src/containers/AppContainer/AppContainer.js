import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Router } from 'react-router-dom';
import { withRouter } from 'react-router';
import { Grid, withStyles } from '@material-ui/core';
import {
  TilesContainer,
  ListContainer,
  ReportContainer,
} from '../../containers';
import { Layout, RouteWithLayout, Header, NotFound } from '../../components';
import * as actions from './actions';
import styles from './styles';
import { makeAppData } from './selectors';

const Routes = (props) => {
  return (
    <Grid container>
      <Switch>
        <RouteWithLayout
          exact
          path="/"
          layout={Layout}
          component={TilesContainer}
        />
        <RouteWithLayout
          title="lists"
          exact
          path="/lists"
          component={TilesContainer}
          layout={Layout}
        />
        <RouteWithLayout
          exact
          path="/lists/:status"
          component={ListContainer}
          layout={Layout}
        />
        <RouteWithLayout
          exact
          path="/lists/:status/report/:id"
          component={ReportContainer}
          layout={Layout}
        />
        {/* Default Page */}
        <RouteWithLayout component={NotFound} layout={Layout} />
      </Switch>
    </Grid>
  );
};
class AppContainer extends Component {
  componentDidMount() {
    this.props.getData();
  }

  render() {
    return (
      <Grid container>
        <Header enableIconButton={false} />
        <Router {...this.props}>
          <Routes />
        </Router>
      </Grid>
    );
  }
}

// Using memoized selectors
const makeMapStateToProps = () => {
  const getAppData = makeAppData();
  return (state, props) => {
    return { ...getAppData(state, props) };
  };
};

AppContainer = withStyles(styles)(AppContainer);
export default withRouter(
  connect(
    makeMapStateToProps,
    { ...actions },
  )(AppContainer),
);
