import { GET_DATA, GET_DATA_FAIL, GET_DATA_SUCCESS } from './constants';

export const getData = (data) => ({
  type: GET_DATA,
  data,
});

export const getDataFail = (message) => ({
  type: GET_DATA_FAIL,
  message,
});

export const getDataSuccess = (data) => ({
  type: GET_DATA_SUCCESS,
  data,
});
