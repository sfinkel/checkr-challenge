export default (theme) => ({
  root: {
    maxWidth: 1020,
    margin: '0 auto',
    flexGrow: 1,
    paddingTop: theme.spacing.unit * 3,
    paddingRight: theme.spacing.unit * 3,
    paddingLeft: theme.spacing.unit * 3,
    paddingBottom: theme.spacing.unit * 3,
  },
});
