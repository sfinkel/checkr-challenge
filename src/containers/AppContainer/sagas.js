import { fetchInitialData } from '../../api/checkr';
import { getDataFail, getDataSuccess } from './actions';
import { GET_DATA } from './constants';
import { call, put, takeLatest } from 'redux-saga/effects';

export function* getData() {
  try {
    const data = yield call(fetchInitialData);

    yield put(getDataSuccess(data));
  } catch ({ message }) {
    yield put(getDataFail(message));
  }
}

export function* sagaWatcher() {
  yield takeLatest(GET_DATA, getData);
}

export default [sagaWatcher];
