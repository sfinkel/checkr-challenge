export default (value = '') =>
  typeof value === 'string' && value.replace(/\D/g, '');
