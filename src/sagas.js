import appSaga from './containers/AppContainer/sagas';
import { all, fork } from 'redux-saga/effects';

export default function* rootSaga() {
  yield all([...appSaga].map(fork));
}
