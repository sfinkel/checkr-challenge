import createSagaMiddleware from 'redux-saga';
import { applyMiddleware, compose, createStore } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import initialState from './initialState';
import reducer from './reducers';
import rootSaga from './sagas';

export default function configureStore(history) {
  const sagaMiddleware = createSagaMiddleware();

  const middleware = [routerMiddleware(history), sagaMiddleware];

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      : compose;

  const enhancer = composeEnhancers(
    applyMiddleware(...middleware),
    // other store enhancers if any
  );

  /* eslint-enable */
  const store = createStore(reducer, initialState, enhancer);

  /*
   *
   * BEGIN CHANGES FOR HOT
   * CHANGED FROM:  //sagaMiddleware.run(rootSaga);
   *
   */
  let sagaTask = sagaMiddleware.run(rootSaga);
  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./reducers', () => {
      const nextRootReducer = require('./reducers');
      store.replaceReducer(nextRootReducer);
    });
    module.hot.accept('./sagas', () => {
      const getNewSagas = rootSaga;
      sagaTask.cancel();
      sagaTask.done.then(() => {
        sagaTask = sagaMiddleware.run(function* replacedSaga(action) {
          yield getNewSagas();
        });
      });
    });
  }
  return store;
}
