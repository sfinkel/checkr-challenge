import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core';

/* Local dependencies */
import registerServiceWorker from './registerServiceWorker';
import configureStore from './store';

/* Overriding theme here */
//import theme from './theme';
import checkrTheme from './checkrTheme';

import './index.css';
import { AppContainer as App } from './containers/AppContainer';

/* create history, store */
const history = createHistory();
const store = configureStore(history);
const theme = createMuiTheme(/*theme*/ checkrTheme);

/* bind react to root element */
ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root'),
);

registerServiceWorker();
