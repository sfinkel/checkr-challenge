const initialState = {
  app: {
    clear: {},
    pending: {},
    suspended: {},
    consider: {},
    isLoading: false,
  },
  routing: {},
};

export default initialState;
