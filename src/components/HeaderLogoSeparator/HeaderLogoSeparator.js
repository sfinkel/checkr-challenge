import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircle } from '@fortawesome/free-solid-svg-icons';
import { withStyles } from '@material-ui/core';
import styles from './styles';

const HeaderLogoSeparator = ({ classes }) => {
  return (
    <div>
      {` `}
      <FontAwesomeIcon
        icon={faCircle}
        transform="shrink-6"
        size="xs"
        className={classes.root}
      />
      {` `}
    </div>
  );
};
export default withStyles(styles)(HeaderLogoSeparator);
