import React, { Component } from 'react';
import { Grid, Typography, withStyles } from '@material-ui/core';
import { CheckrStatusWithIcon } from '../../components';
import styles from './styles';
class CheckrListTileTitle extends Component {
  render() {
    const { count = '', status = '', classes } = this.props;
    return (
      <Grid
        container
        align="center"
        justify="center"
        alignContent="center"
        alignItems="center"
        className={classes.container}
      >
        <Typography
          variant="display1"
          align="center"
          className={classes.countText}
        >
          {count}
        </Typography>
        <Grid
          container
          align="center"
          justify="center"
          alignContent="center"
          alignItems="center"
        >
          <Grid item align="center">
            <CheckrStatusWithIcon
              status={status}
              className={classes.statusWithIconText}
            />
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(CheckrListTileTitle);
