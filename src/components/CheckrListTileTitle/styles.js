export default (theme) => ({
  container: {
    margin: '20px 0px 20px 0px',
  },
  countText: {
    fontSize: '50px',
    color: 'black',
    fontWeight: '200',
  },
  statusWithIconText: {
    fontSize: '14px',
  },
});
