import React, { Component } from 'react';
import { TableCell } from '@material-ui/core';

class ClickableCell extends Component {
  render() {
    const { children, onMouseDown, ...rest } = this.props;
    return (
      <TableCell onMouseDown={onMouseDown} {...rest}>
        {children}
      </TableCell>
    );
  }
}

export default ClickableCell;
