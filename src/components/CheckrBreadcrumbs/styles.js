export default (theme) => ({
  link: {
    textDecoration: 'none',
    color: 'white',
  },
  activeLink: {
    fontWeight: 'bold',
    color: 'white',
    textDecoration: 'none',
  },
});
