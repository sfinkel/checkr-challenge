import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import withBreadcrumbs from 'react-router-breadcrumbs-hoc';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

// breadcrumbs can be any type of component or string
const ListsBreadcrumb = ({ match }) => <span>Lists</span>;
const ReportBreadCrumb = ({ match }) => <span>Report</span>;

// define some custom breadcrumbs for certain routes (optional)
const routes = [
  { path: '/lists', breadcrumb: null },
  { path: '/lists/:status', breadcrumb: ListsBreadcrumb },
  { path: '/lists/:status/report', breadcrumb: ReportBreadCrumb },
  { path: '/lists/:status/report/:reportId', breadcrumb: null },
];

// map & render your breadcrumb components however you want.
// each `breadcrumb` has the props `key`, `location`, and `match` included!
class CheckrBreadcrumbs extends Component {
  render() {
    const {
      classes: { link, activeLink },
      breadcrumbs,
    } = this.props;
    return (
      <div>
        {breadcrumbs.map((breadcrumb, index) => (
          <span key={breadcrumb.key}>
            <NavLink
              className={link}
              to={breadcrumb.props.match.url}
              activeClassName={activeLink}
              isActive={() => breadcrumbs.length - 1 === index}
            >
              {breadcrumb}
            </NavLink>
            {index < breadcrumbs.length - 1 && (
              <span>
                <FontAwesomeIcon
                  icon={faChevronRight}
                  size="xs"
                  transform="shrink-6"
                  style={{ marginRight: '10px', marginLeft: '10px' }}
                />
              </span>
            )}
          </span>
        ))}
      </div>
    );
  }
}

CheckrBreadcrumbs = withStyles(styles)(CheckrBreadcrumbs);
export default withBreadcrumbs(routes)(CheckrBreadcrumbs);
