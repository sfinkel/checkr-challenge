import React, { Component } from 'react';
import { AppBar, IconButton, Toolbar } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import {
  HeaderLogo,
  HeaderLogoSeparator,
  CheckrBreadcrumbs as Breadcrumbs,
} from '../../components';
import styles from './styles';

class Header extends Component {
  render() {
    const {
      classes,
      enableIconButton,
      enableLogo = true,
      enableIconAfterLogo = true,
      enableBreadCrumbs = true,
    } = this.props;
    return (
      <AppBar position="static" className={classes.appBar}>
        <Toolbar className={classes.appBarToolbar}>
          {enableIconButton ? (
            <IconButton
              className={classes.menuButton}
              color="inherit"
              aria-label="Menu"
            >
              <MenuIcon />
            </IconButton>
          ) : null}
          {enableLogo ? <HeaderLogo {...this.props} /> : null}
          {enableIconAfterLogo ? <HeaderLogoSeparator /> : null}
          {enableBreadCrumbs ? <Breadcrumbs /> : null}
        </Toolbar>
      </AppBar>
    );
  }
}

Header = withStyles(styles)(Header);
export default withRouter(
  connect(
    null,
    null,
  )(Header),
);
