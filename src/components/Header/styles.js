export default (theme) => ({
  root: {
    root: {
      flexGrow: 1,
    },
    appBarToolbar: {
      minHeight: '56px !important',
      maxHeight: '56px !important',
    },
    appBar: {
      maxHeight: '56px !important',
      minHeight: '56px !important',
    },
    flex: {
      flex: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: '1%',
    },
  },
});
