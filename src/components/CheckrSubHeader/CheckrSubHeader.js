import React, { Component } from 'react';
import { AppBar, IconButton, Toolbar } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import { connect } from 'react-redux';

class Header extends Component {
  render() {
    const { classes } = this.props;
    return (
      <AppBar position="static" className={classes.appBar}>
        <Toolbar disableGutters={false}>
          <IconButton
            className={classes.menuButton}
            color="inherit"
            aria-label="Menu"
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    );
  }
}

const styles = {
  root: {
    flexGrow: 1,
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: '1%',
  },
  appBar: {
    backgroundImage:
      'linear-gradient(90deg, #017AFF 3%, #006EFF 16%, #059CFF 100%)',
    boxShadow: 'none',
  },
};

Header = withStyles(styles)(Header);
export default connect(
  null,
  null,
)(Header);
