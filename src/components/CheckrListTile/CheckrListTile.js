import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { lowerCase } from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import {
  CheckrListTileFooter,
  CheckrListTileTitle,
  EnhancedTable,
} from '../../components';
import * as selectors from '../../containers/AppContainer/selectors';
import styles from './styles';

class CheckrListTile extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    data: PropTypes.array.isRequired,
    status: PropTypes.string.isRequired,
    columnData: PropTypes.array.isRequired,
  };

  render() {
    const { classes, data = [], status = '', columnData } = this.props;
    return (
      <Grid container className={classes.root}>
        <Grid item xs>
          <EnhancedTable
            columnData={columnData}
            rows={data}
            onClick={this.props.onClickRow}
            enableCheckbox={false}
            tableFooter={<CheckrListTileFooter status={status} />}
            title={
              <CheckrListTileTitle
                status={lowerCase(status)}
                count={`${data.length}`}
              />
            }
            classes={{ tablePaper: classes.tablePaper }}
            style={{ tableLayout: 'fixed' }}
          />
        </Grid>
      </Grid>
    );
  }
}

// memoized selector
const makeMapStateToProps = () => {
  const getTransformedData = selectors.makeTransformedData();
  const getColumnData = selectors.makeTileColumnData();
  return (state, props) => ({
    ...getTransformedData(state, props), // data, status
    columnData: getColumnData(state, props),
  });
};

CheckrListTile = withStyles(styles)(CheckrListTile);
export default connect(makeMapStateToProps)(CheckrListTile);
