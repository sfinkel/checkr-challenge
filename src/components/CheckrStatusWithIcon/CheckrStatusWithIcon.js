import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import { CheckrStatusIcon } from '../../components';
import styles from './styles';
class CheckrStatusWithIcon extends Component {
  render() {
    const { status, classes } = this.props;
    return (
      <Grid container className={classes.root}>
        <Grid item>
          <Typography className={classes.root}>
            <CheckrStatusIcon status={status} className={classes.statusIcon} />
            <span
              className={`${classes[`${status}Text`]} ${classes.statusText}`}
            >
              {status}
            </span>
          </Typography>
        </Grid>
      </Grid>
    );
  }
}

export default withStyles(styles)(CheckrStatusWithIcon);
