export default (theme) => ({
  root: {
    fontWeight: '600',
  },
  statusIcon: {
    marginRight: '5px',
  },
  statusText: {
    textTransform: 'capitalize',
  },
  pendingText: {
    ...theme.pendingIcon,
  },
  paper: { borderRadius: '10px' },
});
