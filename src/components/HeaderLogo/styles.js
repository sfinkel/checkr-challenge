import Logo from './logo.svg';

export default (theme) => ({
  root: {
    backgroundRepeat: 'no-repeat',
    width: '100px',
    height: '30px',
    marginRight: '10px',
    backgroundImage: `url(${Logo})`,
  },
});
