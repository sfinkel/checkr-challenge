import React from 'react';
import { withStyles } from '@material-ui/core';
import styles from './styles';

const MenuBarLogo = ({ classes }) => {
  return <div className={classes.root} />;
};

export default withStyles(styles)(MenuBarLogo);
