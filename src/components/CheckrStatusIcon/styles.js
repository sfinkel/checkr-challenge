export default (theme) => ({
  root: {},
  statusIcon: {
    marginRight: '5px',
  },
  paper: { borderRadius: '10px' },
  ...theme,
});
