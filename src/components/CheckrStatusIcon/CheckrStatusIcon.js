import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { lowerCase } from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faExclamationCircle,
  faSyncAlt,
  faCheckCircle,
} from '@fortawesome/free-solid-svg-icons';
import styles from './styles';

class CheckrStatusIcon extends Component {
  mapping = (status = '') => {
    const mapping = {
      consider: [{ icon: faExclamationCircle }],
      pending: [{ icon: faSyncAlt }],
      suspended: [{ icon: faExclamationCircle }],

      clear: [{ icon: faCheckCircle }],
    };
    return mapping[status] || [{}];
  };

  render() {
    const { classes = {}, status = '' } = this.props;

    const statusLower = lowerCase(status);
    return status ? (
      <FontAwesomeIcon
        {...Object.assign(...[...this.mapping(statusLower)])}
        {...this.props}
        className={`${classes[`${status}Icon`]} ${classes.statusIcon}`}
      />
    ) : null;
  }
}
export default withStyles(styles)(CheckrStatusIcon);
