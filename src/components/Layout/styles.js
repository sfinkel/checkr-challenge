export default ({ spacing: { unit }, maxWidth }) => ({
  root: {
    margin: '0 auto',
    maxWidth,
  },
});
