import React, { Component } from 'react';
import { Grid, withStyles } from '@material-ui/core';
import styles from './styles';
export class Layout extends Component {
  render() {
    const { children, classes } = this.props;
    return (
      <Grid
        container
        justify={'center'}
        direction={'row'}
        className={classes.root}
      >
        {children}
      </Grid>
    );
  }
}

export default withStyles(styles)(Layout);
