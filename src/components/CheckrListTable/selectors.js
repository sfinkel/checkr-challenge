import { createSelector } from 'reselect';
import { map, orderBy, startCase } from 'lodash';
import moment from 'moment';
export {
  makeTransformedData,
  getStatus,
} from '../../containers/AppContainer/selectors';

const getDataFromProps = (state, props = {}) => {
  return props.data;
};

export const getPropsSelector = createSelector(
  getDataFromProps,
  (data) => data,
);

export const makeData = createSelector(getPropsSelector, (data) => {
  const rows = orderBy(
    map(data, (item) => {
      const {
        item: { name },
        report: { id, status, package: pkg, geo, updated_at: date },
      } = item;
      return {
        name,
        id,
        status,
        pkg: startCase(pkg),
        geo,
        date:
          status === 'pending'
            ? moment(date).fromNow()
            : moment(date).format('MM/DD/YYYY'),
      };
    }),
    ['date'],
    ['desc'],
  );
  return rows;
});
