import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { CheckrStatusWithIcon, EnhancedTable } from '../../components';
import { makeData, getStatus } from './selectors';

import styles from './styles';

class CheckrListTable extends React.Component {
  render() {
    const { onClickRow, title, classes, rows = [], status } = this.props;

    return (
      <EnhancedTable
        columnData={columnData(status)}
        rows={rows}
        onClick={onClickRow}
        rowsPerPage={20}
        enableCheckbox={false}
        paginationEnabled={true}
        title={title}
        classes={{ tablePaper: classes.tablePaper }}
      />
    );
  }
}

const columnData = (status) => [
  {
    id: 'status',
    numeric: false,
    disablePadding: true,
    label: 'STATUS',
    formatter: (status) => <CheckrStatusWithIcon status={status} />,
  },
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'NAME',
  },
  {
    id: 'date',
    numeric: false,
    disablePadding: true,
    label: status === 'pending' ? 'ESTIMATED DELIVERY' : 'UPDATED',
  },
  {
    id: 'pkg',
    numeric: false,
    disablePadding: true,
    label: 'PACKAGE',
  },
  {
    id: 'geo',
    numeric: false,
    disablePadding: true,
    label: 'GEO',
  },
];

const makeMapStateToProps = (state, props) => ({
  rows: makeData(state, props),
  status: getStatus(state, props),
});

CheckrListTable = withStyles(styles)(CheckrListTable);
export default connect(
  makeMapStateToProps,
  null,
)(CheckrListTable);
