export default (theme) => ({
  root: {
    textAlign: 'center',
    borderBottom: 'none',
    color: theme.palette.primary.main,
  },
  column: {
    borderBottom: 'none',
    textAlign: 'center',
    borderRadius: '0 0 10px 10px',
  },
});
