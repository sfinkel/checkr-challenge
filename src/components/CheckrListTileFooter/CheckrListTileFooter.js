import React, { Component } from 'react';
import { TableRow, Typography, withStyles } from '@material-ui/core';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { startCase } from 'lodash';
import { ClickableCell } from '../../components';
import styles from './styles';

class CheckrListTileFooter extends Component {
  onClick = () => {
    const { history, status } = this.props;
    return history.push(`/lists/${status}`);
  };

  render() {
    const { status, classes } = this.props;
    return (
      <TableRow hover className={classes.root}>
        <ClickableCell
          colSpan={2}
          className={classes.column}
          onClick={this.onClick}
        >
          <Typography className={classes.root}>
            {`Show ${startCase(status)} reports`}
          </Typography>
        </ClickableCell>
      </TableRow>
    );
  }
}

CheckrListTileFooter = withStyles(styles)(CheckrListTileFooter);
export default withRouter(
  connect((state, { status }) => ({ status }))(CheckrListTileFooter),
);
