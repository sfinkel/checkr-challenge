import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './styles';

class CheckrStatusIcon extends Component {
  render() {
    const { classes = {}, status = '' } = this.props;

    return status ? (
      <span className={`${classes[`${status}Text`]} ${classes.statusText}`}>
        {status}
      </span>
    ) : null;
  }
}
export default withStyles(styles)(CheckrStatusIcon);
