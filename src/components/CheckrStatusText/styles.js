export default (theme) => ({
  root: {},
  statusText: {
    textTransform: 'upper-case',
  },
  paper: { borderRadius: '10px' },
  ...theme,
});
