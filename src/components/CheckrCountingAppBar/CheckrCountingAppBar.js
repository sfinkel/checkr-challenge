import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import {
  Button,
  Menu,
  Chip,
  MenuItem,
  Grid,
  ListItemText,
} from '@material-ui/core';
import { map, startCase, keys } from 'lodash';
import * as selectors from '../../containers/AppContainer/selectors';
import styles from './styles';
class CheckrCountingAppBar extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = (ev, status) => {
    if (status === 'all') {
      return this.props.history.push(`/lists`);
    }
    if (keys(this.props.statusCounts).includes(status)) {
      this.props.history.push(`/lists/${status}`);
    }
    this.setState({ anchorEl: null });
  };

  render() {
    const { status: currentStatus, classes, statusCounts } = this.props;
    const { anchorEl } = this.state;
    return (
      <Grid container className={classes.root}>
        <Button
          aria-owns={anchorEl ? 'simple-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}
          className={classes.activeButton}
        >
          {`${currentStatus} Reports`}
          <Chip
            label={statusCounts[currentStatus]}
            className={classes.activeChip}
          />
          <FontAwesomeIcon
            icon={faChevronDown}
            style={{ marginLeft: '10px' }}
          />
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {map(statusCounts, (statusCount, status) => {
            return (
              <MenuItem
                selected={currentStatus === status}
                onClick={(ev) => this.handleClose(ev, status)}
                className={classes.menuItem}
                key={status}
              >
                <ListItemText
                  primary={`${startCase(status)} Reports`}
                  disableTypography={true}
                />
                <Chip
                  label={statusCount}
                  className={
                    currentStatus === status ? classes.activeChip : classes.chip
                  }
                />
              </MenuItem>
            );
          })}
        </Menu>
      </Grid>
    );
  }
}

// memoized selector
const makeMapStateToProps = () => {
  const getStatusCounts = selectors.makeStatusCounts();
  return (state, props) => ({
    status: selectors.getStatus(state, props),
    statusCounts: getStatusCounts(state, props),
  });
};
CheckrCountingAppBar = withStyles(styles)(CheckrCountingAppBar);
export default withRouter(
  connect(
    makeMapStateToProps,
    null,
  )(CheckrCountingAppBar),
);
