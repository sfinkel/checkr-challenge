export default (theme) => ({
  root: {
    paddingTop: '33px',
  },
  menuItem: {
    '&:focus': {
      backgroundColor: 'white !important',
      color: theme.palette.primary.main,
    },
    '&:hover': {
      //color: theme.palette.primary.main,   // leaving for reference
      //backgroundColor: 'white !important', // leaving for reference
    },
  },
  activeText: {
    color: `blue !important`,
  },

  activeChip: {
    backgroundColor: theme.palette.primary.main,
    color: 'white',
    marginLeft: '10px',
    float: 'right',
    padding: '0px 15px 0px 15px',
    maxWidth: '30px',
  },

  chip: {
    marginLeft: '10px',
    padding: '0px 15px 0px 15px',
    maxWidth: '30px',
  },

  activeButton: {
    textTransform: 'capitalize',
    color: theme.palette.primary.main,
  },

  primary: {},
  icon: {},
});
