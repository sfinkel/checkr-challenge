import React from 'react';
import { withStyles, Grid } from '@material-ui/core/styles';
import styles from './styles';

const Footer = ({ classes }) => (
  <Grid container className={classes.root}>
    <Grid item xs={3}>
      A
    </Grid>
    <Grid item xs={3}>
      Demo
    </Grid>
    <Grid item xs={3}>
      App
    </Grid>
    <Grid item xs={3}>
      Copyright &copy; 2018 All Rights Reserved
    </Grid>
  </Grid>
);

export default withStyles(styles)(Footer);
