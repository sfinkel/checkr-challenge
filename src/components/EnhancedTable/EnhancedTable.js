import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  TablePagination,
  Checkbox,
  Paper,
  Grid,
  TableFooter,
} from '@material-ui/core';
import map from 'lodash/map';
import EnhancedTableHead from './EnhancedTableHead';
import EnhancedTableToolbar from './EnhancedTableToolbar';
import styles from './styles';

class EnhancedTable extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      order: 'desc',
      orderBy: 'date',
      selected: [],
      rows: [],
      page: 0,
      rowsPerPage: props.rowsPerPage ? props.rowsPerPage : 5,
    };
  }

  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';
    const { rows = [] } = this.props;

    if (this.state.orderBy === property && this.state.order === 'desc') {
      order = 'asc';
    }
    const data =
      order === 'desc'
        ? rows.sort((a, b) => (b[orderBy] < a[orderBy] ? -1 : 1))
        : rows.sort((a, b) => (a[orderBy] < b[orderBy] ? -1 : 1));
    this.setState({ data, order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      this.setState({ selected: this.state.rows.map((n) => n.id) });
      return;
    }
    this.setState({ selected: [] });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value });
  };

  isSelected = (id) => this.state.selected.indexOf(id) !== -1;

  render() {
    const {
      rows,
      classes,
      columnData,
      onClick,
      title,
      paginationEnabled,
      enableCheckbox = true,
      enableEmptyRows = false,
      enableHeaderLabels = true,
      enableHeader = true,
      elevation = 2,
    } = this.props;

    const { order, orderBy, selected, rowsPerPage, page } = this.state;
    const emptyRows =
      rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
    return (
      <Grid container className={classes.root}>
        <Grid item className={classes.root}>
          <Paper elevation={elevation} className={classes.tablePaper}>
            <EnhancedTableToolbar
              numSelected={selected.length}
              title={title}
              enableCheckbox={enableCheckbox}
              className={classes}
            />
            <Table className={classes.table} aria-labelledby="tableTitle">
              <EnhancedTableHead
                numSelected={selected.length}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={rows.length}
                columns={columnData}
                {...{
                  order,
                  orderBy,
                  enableCheckbox,
                  enableHeaderLabels,
                  enableHeader,
                }}
              />
              <TableBody className={classes.body}>
                {rows
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((data, index) => {
                    const { id } = data;
                    const isSelected = enableCheckbox
                      ? this.isSelected(id)
                      : null;

                    return (
                      <TableRow
                        hover
                        onClick={(event) =>
                          onClick
                            ? onClick(event, id)
                            : this.handleClick(event, id)
                        }
                        role="checkbox"
                        aria-checked={isSelected}
                        tabIndex={-1}
                        key={id || index}
                        selected={isSelected}
                      >
                        {enableCheckbox ? (
                          <TableCell padding="checkbox">
                            <Checkbox checked={isSelected} />
                          </TableCell>
                        ) : null}
                        {map(columnData, ({ id, formatter }) => {
                          const value = data[id];
                          return (
                            <TableCell
                              key={`${index}-${value}`}
                              scope="row"
                              padding={enableCheckbox ? '0px' : 'checkbox'}
                            >
                              {!formatter ? value : formatter(value)}
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    );
                  })}
                {enableEmptyRows &&
                  emptyRows > 0 && (
                    <TableRow style={{ height: 49 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
              </TableBody>
              <TableFooter hover="true" className={classes.tableFooter}>
                {this.props.tableFooter ? this.props.tableFooter : null}
              </TableFooter>
            </Table>
            {paginationEnabled ? (
              <TablePagination
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                  'aria-label': 'Next Page',
                }}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
              />
            ) : null}
          </Paper>
        </Grid>
      </Grid>
    );
  }
}

EnhancedTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EnhancedTable);
