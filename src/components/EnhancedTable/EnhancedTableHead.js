import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import {
  TableHead,
  TableCell,
  TableRow,
  TableSortLabel,
  Checkbox,
  Tooltip,
} from '@material-ui/core';

class EnhancedTableHead extends Component {
  static propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
  };
  createSortHandler = (property) => (event) => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount,
      columns = [],
      enableCheckbox = true,
      enableHeaderLabels = true,
      enableHeader = true,
    } = this.props;
    return enableHeader ? (
      <TableHead>
        <TableRow>
          {enableCheckbox ? (
            <TableCell padding="checkbox">
              <Checkbox
                indeterminate={numSelected > 0 && numSelected < rowCount}
                checked={numSelected === rowCount}
                onChange={onSelectAllClick}
              />
            </TableCell>
          ) : null}
          {columns.map(({ id, numeric, disablePadding, label }) => {
            return enableHeaderLabels ? (
              <TableCell
                key={id}
                numeric={numeric}
                padding={
                  disablePadding
                    ? enableCheckbox
                      ? 'none'
                      : 'checkbox'
                    : 'default'
                }
                sortDirection={orderBy === id ? order : false}
              >
                <Tooltip
                  title="Sort"
                  placement={numeric ? 'bottom-end' : 'bottom-start'}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === id}
                    direction={order}
                    onClick={this.createSortHandler(id)}
                  >
                    {label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            ) : null;
          }, this)}
        </TableRow>
      </TableHead>
    ) : null;
  }
}

const tableHeadStyles = (theme) => ({});
export default withStyles(tableHeadStyles)(EnhancedTableHead);
