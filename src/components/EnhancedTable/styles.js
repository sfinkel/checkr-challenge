export default (theme) => ({
  root: {
    width: '100%',
  },
  table: {},
  tableFooter: {
    '&:hover:hover': {
      cursor: 'pointer',
    },
  },
  body: {
    '&:hover:hover': {
      cursor: 'pointer',
    },
  },
  tablePaper: {},
  tableWrapper: {
    overflowX: 'auto',
  },
  paper: {},
  input: {},
  card: {},
});
